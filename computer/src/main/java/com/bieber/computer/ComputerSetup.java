package com.bieber.computer;

import com.bieber.common.Utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by bieber on 2015/9/5.
 */
public class ComputerSetup {

    public static void main(String[] args){
        if(args.length<=0){
            throw new IllegalArgumentException("args must not be null");
        }
        ComputerConfig computerConfig = new ComputerConfig();
        computerConfig.loadFromProperties(Utils.loadProperties(args[0]));
        ComputerServer server = new ComputerServer();
        server.start(computerConfig);
    }
}
