package com.bieber.computer;

import com.bieber.common.AbstractConfig;

/**
 * Created by bieber on 2015/9/5.
 */
public class ComputerConfig extends AbstractConfig {
    
    private int preNodeSize=1000;
    
    private String personCSVFile;

    private String orderCSVFile;

    public String getPersonCSVFile() {
        return personCSVFile;
    }

    public void setPersonCSVFile(String personCSVFile) {
        this.personCSVFile = personCSVFile;
    }

    public String getOrderCSVFile() {
        return orderCSVFile;
    }

    public void setOrderCSVFile(String orderCSVFile) {
        this.orderCSVFile = orderCSVFile;
    }

    public int getPreNodeSize() {
        return preNodeSize;
    }

    public void setPreNodeSize(int preNodeSize) {
        this.preNodeSize = preNodeSize;
    }
}

