package com.bieber.imdg;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.Random;

/**
 * Created by bieber on 2015/9/17.
 */
public class CreateCSV {
    
    public static void main(String[] args){
        createPersonCSV(10000);
        createOrderCSV(100000,10000);
    }
    private static void createOrderCSV(int orderSize,int personSize){
        BufferedWriter writer = null;
        try {
            writer= new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File("C:\\Users\\bieber\\working\\imdg\\order.csv"))));
            StringBuffer stringBuffer = new StringBuffer();
            Random random = new Random(System.currentTimeMillis());
            for(int i=0;i<orderSize;i++){
                stringBuffer.append(i).append(",").append(random.nextInt(personSize)).append(",").append(random.nextDouble()*100);
                writer.write(stringBuffer.toString());
                writer.newLine();
                stringBuffer.setLength(0);
            }
            writer.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            IOUtils.closeQuietly(writer);
        }
    }
    private static void createPersonCSV(int size){
        BufferedWriter writer = null;
        try {
            writer= new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File("C:\\Users\\bieber\\working\\imdg\\person.csv"))));
            StringBuffer stringBuffer = new StringBuffer();
            for(int i=0;i<size;i++){
                stringBuffer.append(i).append(",").append("personName").append(i);
                writer.write(stringBuffer.toString());
                writer.newLine();
                stringBuffer.setLength(0);
            }
            writer.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            IOUtils.closeQuietly(writer);
        }
    }
}
