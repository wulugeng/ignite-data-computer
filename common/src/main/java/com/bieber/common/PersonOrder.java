package com.bieber.common;

import com.alibaba.fastjson.JSON;
import com.bieber.common.model.CacheValue;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

/**
 * Created by bieber on 2015/9/5.
 */
public class PersonOrder extends CacheValue<PersonOrder> {
    @QuerySqlField(index = true)
    private Integer id;
    @QuerySqlField
    private long count;
    @QuerySqlField
    private Double amount;
    @QuerySqlField
    private String name;



    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toJsonString() {
        return JSON.toJSONString(this);
    }

    @Override
    public String toString() {
        return "PersonOrder{" +
                "id=" + id +
                ", count=" + count +
                ", amount=" + amount +
                ", name='" + name + '\'' +
                '}';
    }
}
