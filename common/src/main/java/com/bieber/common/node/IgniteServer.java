package com.bieber.common.node;

import com.bieber.common.AbstractConfig;
import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;

/**
 * Created by bieber on 2015/9/5.
 */
public abstract class IgniteServer<T extends AbstractConfig> {

    protected static Ignite ignite;

    protected boolean isServer;

    public IgniteServer(boolean isServer) {
        this.isServer = isServer;
    }

    protected abstract void doStart(T config);

    public abstract void stop(T config);

    public void start(T config){
        if(!isServer){
            Ignition.setClientMode(true);
        }
        ignite = Ignition.start(IgniteServer.class.getResource("/ignite-server.xml"));
        ignite.cluster().withAsync();
        doStart(config);
    }

    public static Ignite getCurrentIgnite(){
        if(ignite==null){
            throw new IllegalStateException("node start ignite node");
        }
        return ignite;
    }

}
