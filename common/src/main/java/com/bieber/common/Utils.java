package com.bieber.common;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.Properties;

/**
 * Created by bieber on 2015/9/17.
 */
public class Utils {
    
    public static Properties loadProperties(String propertiesFile){
        File file = new File(propertiesFile);
        if(!file.exists()){
            throw new IllegalArgumentException("file "+propertiesFile+" not exist");
        }
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
            Properties properties = new Properties();
            properties.load(inputStream);
            return properties;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("load properties file "+propertiesFile+" occur an exception",e);
        } catch (IOException e) {
            throw new IllegalArgumentException("load properties file "+propertiesFile+" occur an exception",e);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }

    }
}
