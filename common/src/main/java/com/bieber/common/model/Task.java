package com.bieber.common.model;

import java.io.Serializable;

/**
 * Created by bieber on 2015/9/18.
 */
public class Task implements Serializable{
    
    private String orderCacheName;
    
    private int personSize;

    public String getOrderCacheName() {
        return orderCacheName;
    }

    public void setOrderCacheName(String orderCacheName) {
        this.orderCacheName = orderCacheName;
    }

    public int getPersonSize() {
        return personSize;
    }

    public void setPersonSize(int personSize) {
        this.personSize = personSize;
    }
}
