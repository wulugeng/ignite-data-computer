package com.bieber.common.model;

import com.alibaba.fastjson.JSON;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.io.Serializable;

/**
 * Created by bieber on 2015/9/4.
 */
public class ConsumerPerson extends CacheValue<ConsumerPerson> implements Serializable{

    @QuerySqlField(index = true,descending = true)
    private Integer id;

    @QuerySqlField
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toJsonString() {
        return JSON.toJSONString(this);
    }

    @Override
    public String toString() {
        return "ConsumerPerson{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
