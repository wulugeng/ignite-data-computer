package com.bieber.common.model;

import com.alibaba.fastjson.JSON;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.io.Serializable;

/**
 * Created by bieber on 2015/9/4.
 */
public class BizOrder extends CacheValue<BizOrder> implements Serializable{
    @QuerySqlField(index = true)
    private Integer id;
    @QuerySqlField(orderedGroups = {@QuerySqlField.Group(name="biz_order_persion_idx",order = 0,descending = true)})
    private Integer personId;
    @QuerySqlField
    private Double amount;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toJsonString() {
        return JSON.toJSONString(this);
    }

    @Override
    public String toString() {
        return "BizOrder{" +
                "id=" + id +
                ", personId=" + personId +
                ", amount=" + amount +
                '}';
    }
}
