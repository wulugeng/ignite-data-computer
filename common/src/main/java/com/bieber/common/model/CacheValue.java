package com.bieber.common.model;

import com.alibaba.fastjson.JSON;

/**
 * Created by bieber on 2015/9/4.
 */
public abstract class CacheValue<T extends CacheValue> {

    public abstract int getId();

    public abstract String toJsonString();

    public final static CacheValue toPojo(String jsonString,Class<? extends CacheValue> type){
        return JSON.parseObject(jsonString,type);
    }
}
