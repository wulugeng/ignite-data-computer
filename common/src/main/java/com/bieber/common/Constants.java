package com.bieber.common;


import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by bieber on 2015/9/4.
 */
public class Constants {

    public static final String PERSON_CACHE = "IgnitePerson";

    public static final String ORDER_CACHE = "IgniteOrder";

    public static final String PERSON_ORDER_CACHE ="IgnitePersonOrder";

    public static final String CHAR_SET="UTF-8";

    public static final byte[] DELIM = "\n\r".getBytes();

    public static final Logger COMMON_LOGGER = LoggerFactory.getLogger("ignite-data-compute");

}
