package com.bieber.node.pojo;

import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by bieber on 2015/9/6.
 */
public class Organization {

    private static final AtomicInteger atomic = new AtomicInteger();
    /** Organization ID (indexed). */
    @QuerySqlField(index = true)
    private Integer id;

    /** Organization name (indexed). */
    @QuerySqlField(index = true)
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Create organization.
     *
     * @param name Organization name.
     */
    public Organization(String name) {
        id = atomic.getAndIncrement();

        this.name = name;
    }

    /** {@inheritDoc} */
    @Override public String toString() {
        return "Organization [id=" + id + ", name=" + name + ']';
    }
}
