package com.bieber.node;

import com.bieber.common.*;
import com.bieber.common.model.BizOrder;
import com.bieber.common.model.ConsumerPerson;
import com.bieber.common.node.IgniteServer;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.configuration.CacheConfiguration;

/**
 * Created by bieber on 2015/9/5.
 */
public class GridNode extends IgniteServer {

    public GridNode() {
        super(true);
    }

    @Override
    protected void doStart(AbstractConfig config) {
        CacheConfiguration<Integer,PersonOrder> personOrderCacheConfiguration = new CacheConfiguration<Integer, PersonOrder>(Constants.PERSON_ORDER_CACHE);
        personOrderCacheConfiguration.setCacheMode(CacheMode.PARTITIONED);
        personOrderCacheConfiguration.setBackups(1);
        personOrderCacheConfiguration.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
        personOrderCacheConfiguration.setIndexedTypes(Integer.class,PersonOrder.class);
        ignite.getOrCreateCache(personOrderCacheConfiguration);
    }

    @Override
    public void stop(AbstractConfig config) {

    }
}
